# Track news stories from news articles

This project is the front-end of the [`document_tracking`](https://gitlab.univ-lr.fr/cross-lingual-event-tracking/developpement/from-documents-to-events/document_tracking) package, which proposes algorithms to track news documents (long articles, telegraphic dispatches, etc.) into news stories (group of documents reporting similar events). It manipulates data following the [`document_tracking_resources`](https://gitlab.univ-lr.fr/cross-lingual-event-tracking/developpement/from-documents-to-events/documents_tracking_resources) format, and all your datasets should complain with it.

## Installation

```bash
pip install news_tracking
```

## Utilities

Once installed, you will be provided new commands which act as a front-end to the utilities your need.

* `news_tracking_miranda`: run the Miranda et al. algorithm (see the `document_tracking` package for more information). on you datasets. It needs to have a model, as the algorithm is supervised. 
* `news_tracking_miranda_training`: train and export a model for the Miranda et al. algorithm.
* `news_tracking_kmeans`: run K-Means on a dataset, it can act as a baseline algorithm.
* `news_tracking_evaluation`: evaluate the clustering of algorithms using Standard and BCubed metrics.

